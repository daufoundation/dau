FROM golang:latest

WORKDIR  $GOPATH/src/dau
COPY . .
RUN rm .env
RUN rm 2.env
RUN go get -d -v ./...
RUN go install -v ./...

RUN mkdir $GOPATH/src/dau/data
RUN mkdir $GOPATH/src/dau/build
COPY /dev-servers/server1/.env $GOPATH/src/dau/build
COPY /dev-servers/server1/.env .
COPY server_list.json $GOPATH/src/dau/build
# COPY go.mod ./
# COPY go.sum ./
# COPY *.go ./
# RUN go mod vendor
# RUN go mod tidy


RUN GOOS=linux go build -ldflags="-s -w" -o "/build/LID-server" app.go keys.go

EXPOSE 300

CMD /build/LID-server
# RUN cp .env.example $BUILD_DIR/linux/.env.example
# RUN cp server_list.example $BUILD_DIR/linux/server_list.example.json
# RUN cp lid-network.service $BUILD_DIR/linux/lid-network.serviced

#docker build -t dau .
#docker run -p 8080:8080 -tid dau
# docker exec -t dau bash