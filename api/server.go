package api

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"time"

	"dau/api/wallet"
	"dau/blockchain"
	"dau/database"
	"dau/node"
	"dau/utils"
	"dau/utils/limitter"
	"dau/utils/logging"
	"github.com/gorilla/mux"
)

var (
	buildIDHeader        = "X-LID-BUILD"
	broadCastHeader      = "X-LID-Outbound"
	defaultGossipSpacing = 3 * time.Minute
	defaultPeerSampling  = 3
	peerListFile         = "server_list.json"
)

// Server represents the HTTP Server
type Server struct {
	log                     logging.Logger
	logFactory              logging.Factory
	config                  *node.Config
	listenAddress           string
	r                       *mux.Router
	srv                     *http.Server
	chain                   *blockchain.Handler
	priceDB, transactionsDB database.Database
}

// Initialize the http server
func (s *Server) Initialize(
	log logging.Logger,
	logFactory logging.Factory,
	config *node.Config,
	priceDB, transactionsDB database.Database,
) error {
	s.config = config
	s.logFactory = logFactory
	//s.listenAddress = fmt.Sprintf("%s:%s", config.HTTPHost, config.HTTPPort)
	s.listenAddress = fmt.Sprintf(":%s", config.HTTPPort)
	s.r = mux.NewRouter()

	httpLog, err := logFactory.AddDir("HTTP Server", "", true)
	if err != nil {
		return err
	}

	chain, err := blockchain.NewHandler(logFactory, utils.ConnAddr(config))
	if err != nil {
		return err
	}
	s.chain = chain
	s.log = httpLog
	s.priceDB = priceDB
	s.transactionsDB = transactionsDB
	return nil
}

// Dispatch registers all routes and starts the HTTP server
func (s *Server) Dispatch() error {
	l, err := net.Listen("tcp", s.listenAddress)
	if err != nil {
		return err
	}

	// apply rate limitting middleware
	rlimitter := limitter.New(s.log, s.config.MaxBucketSize, s.config.RateLimitCleanupInterval)
	go rlimitter.Cleanup()

	handler := rlimitter.Limit(s.r)
	err = s.initRoutes()
	if err != nil {
		return err
	}

	s.srv = &http.Server{Handler: handler}

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)

	go func() {
		<-quit

		if err := s.Shutdown(); err != nil {
			s.log.Error("failed To ShutDown Server", err)
		}

		s.log.Info("Shutting down HTTP Server")
	}()

	s.log.Info("Running server on %s", s.listenAddress)
	return s.srv.Serve(l)
}

func (s *Server) initRoutes() error {
	// register handlers
	walletsLogger, err := s.logFactory.AddDir("Wallet API", "", true)
	if err != nil {
		return err
	}
	wallets := wallet.NewService(walletsLogger, s.config, s.transactionsDB)
	s.r.Use(accessControlMiddleware)
	nr := s.r.PathPrefix("/node").Subrouter() // Node routers
	s.r = s.r.PathPrefix("/api/v1").Subrouter()
	wr := s.r.PathPrefix("/wallet").Subrouter()

	s.r.HandleFunc("/blockchain", s.ArchiveBlocks).Methods("GET","OPTIONS")
	s.r.HandleFunc("/price", s.GetPrice).Methods("GET","OPTIONS")
	s.r.HandleFunc("/price/history", s.GetPriceHistory).Methods("GET","OPTIONS")

	wr.HandleFunc("/balance", wallets.GetBalance).Methods("GET","OPTIONS")
	wr.HandleFunc("/create", wallets.CreateWallet).Methods("POST","OPTIONS")
	wr.HandleFunc("/print", wallets.PrintBlocks).Methods("POST","OPTIONS")
	wr.HandleFunc("/info", wallets.WalletInfo).Methods("POST","OPTIONS")
	wr.HandleFunc("/transfer", wallets.TransferLID).Methods("POST","OPTIONS")
	// broadacasted channels
	nr.Use(s.extractAndValidateBuildID)
	nr.HandleFunc("/gossip-peer", s.AcceptPeerGossip).Methods("POST","OPTIONS")
	nr.HandleFunc("/sync", s.AcceptPeerSync).Methods("POST","OPTIONS")
	nr.HandleFunc("/sync-r", s.ProcessSync).Methods("POST","OPTIONS")

	wb := wr.PathPrefix("/node").Subrouter()
	wb.Use(s.extractAndValidateBuildID)

	wb.HandleFunc("/balance", wallets.GetBalance).Methods("GET","OPTIONS")
	wb.HandleFunc("/create", wallets.CreateWallet).Methods("POST","OPTIONS")
	wb.HandleFunc("/print", wallets.PrintBlocks).Methods("POST","OPTIONS")
	wb.HandleFunc("/transfer", wallets.TransferLID).Methods("POST","OPTIONS")

	return nil
}

func (s *Server) extractAndValidateBuildID(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		isBroadcasted := r.Header.Get(broadCastHeader)
		buildID := r.Header.Get(buildIDHeader)

		if isBroadcasted != "" && buildID != "" {
			// compare with current node build
			if buildID != s.config.BuildHash {
				s.log.Error("Invalid node requested with ID %s, killing request", buildID)
				http.Error(w, "Connection not allowed", http.StatusForbidden)
				return
			}
		} else {
			s.log.Error("Invalid node requested with ID %s, killing request", buildID)
			http.Error(w, "Connection not allowed", http.StatusForbidden)
			return
		}

		next.ServeHTTP(w, r)
	})
}

func accessControlMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		header := w.Header()
		header.Add("Access-Control-Allow-Origin", "http://localhost:3000")
		header.Add("Access-Control-Allow-Methods", "GET, POST, OPTIONS,PUT")
		header.Add("Access-Control-Allow-Headers", "X-Requested-With, Access-Control-Allow-Origin, Content-Type, Authorization")

		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusOK)
			return
		}
		next.ServeHTTP(w, r)
	})
}

// Shutdown the HTTP Server
func (s *Server) Shutdown() error {
	if s.srv == nil {
		return nil
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	return s.srv.Shutdown(ctx)
}
