package main

import (
	"errors"
	"flag"
	"fmt"
	"os"
	"time"

	"dau/api"
	"dau/database"
	"dau/database/fs"
	"dau/models"
	"dau/node"
	"dau/utils"
	safebytes "dau/utils/bytes"

	// "dau/blockchain"

	"dau/utils/janitor"
	"dau/utils/logging"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

var (
	defaultLogConfig = logging.Config{
		Directory: logging.DefaultLogDir,
	}
)
var logFactory logging.Factory = logging.NewFactory(defaultLogConfig)
var log, err = logFactory.Build()

func main() {

	if err != nil {
		fmt.Printf("failed to register logger, err: %s", err)
		return
	}

	config, err := parseConfig()
	if err != nil {
		log.Fatal(err.Error())
		return
	}

	// run server
	run(log, logFactory, config)

}

// Initialize and run the node.
func run(
	log logging.Logger,
	logFactory logging.Factory,
	config *node.Config,
) {
	// init node
	n := node.Node{Config: config}
	if err := n.Initialize(config, log, logFactory); err != nil {
		log.Error("failed to initialize node: %v", err)
		return
	}

	// init dbs
	priceDB, transactionsDB, err := initDBs(log, config)
	if err != nil {
		log.Error("%v", err)
		return
	}

	log.Info("dispatching node services")
	err = n.Dispatch()
	if err != nil {
		log.Error("node dispatch failed: %s", err)
	}

	log.Info("Build Hash is: %s", n.Config.BuildHash)

	// init price updater job
	go n.PriceUpdater(priceDB, transactionsDB)

	// Init HTTP Server & APIs
	s := &api.Server{}
	if err := s.Initialize(log, logFactory, config, priceDB, transactionsDB); err != nil {
		log.Error("failed to initiate HTTP Server, %s", err)
		return
	}

	log.Info("Initializing API Server")
	if err := s.Dispatch(); err != nil {
		log.Error("failed to start HTTP Server, %s", err)
		return
	}

	// init block janitor
	initJanitor()
}

func initDBs(log logging.Logger, config *node.Config) (database.Database, database.Database, error) {
	// seed price data
	priceDB, err := fs.New(config.PriceDB)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to initialize priceDB: %v", err)
	}

	log.Info("Initializing priceDB")
	e, err := priceDB.Has([]byte("price"))
	if err != nil {
		return nil, nil, fmt.Errorf("failed to retrieve price from priceDB: %w", err)
	}

	if !e {
		// create genesis price
		price := 0.025
		b := safebytes.Float64bytes(price)
		err := priceDB.Put([]byte("price"), b)
		if err != nil {
			return nil, nil, fmt.Errorf("failed to init price: %v", err)
		}

		// create initial series data
		initialPriceSeries := models.PriceSeriesData{
			Data: [][]interface{}{
				{int64(time.Now().Unix()), 0.0},
				{int64(time.Now().Unix()), price},
			},
		}

		pb, err := safebytes.MarshalBytes(initialPriceSeries)
		if err != nil {
			return nil, nil, fmt.Errorf("failed to init price series data: %v", err)
		}

		if err := priceDB.Put([]byte("price_series"), pb); err != nil {
			return nil, nil, fmt.Errorf("failed to init price series data: %v", err)
		}
	}

	// transactionsDB keeps track of total transactions carried out on this Node
	log.Info("Initializing transactionsDB")
	transactionsDB, err := fs.New("transactions.fdb")
	if err != nil {
		return nil, nil, fmt.Errorf("failed to init transactions db: %v", err)
	}

	e, err = transactionsDB.Has([]byte("transactions_count"))
	if err != nil {
		return nil, nil, fmt.Errorf("failed to retrieve transactions_count from transactionsDB: %w", err)
	}

	if !e {
		// create default transactions_count
		count := float64(0)
		b := safebytes.Float64bytes(count)
		err := transactionsDB.Put([]byte("transactions_count"), b)
		if err != nil {
			return nil, nil, fmt.Errorf("failed to init transactions_count: %v", err)
		}
	}

	return priceDB, transactionsDB, nil
}

func parseConfig() (*node.Config, error) {
	v := viper.New()
	fs := lidNodeFlagSet()

	pflag.CommandLine.AddGoFlagSet(fs)
	pflag.Parse()
	if err := v.BindPFlags(pflag.CommandLine); err != nil {
		return nil, err
	}

	log.Error("reading .env")
	v.SetConfigFile(".env")
	if err := v.ReadInConfig(); err != nil {
		log.Error("Cannot read .env file")
		return nil, err
	}

	config, err := setNodeConfig(v)
	if err != nil {
		return nil, err
	}

	return config, nil
}

func setNodeConfig(v *viper.Viper) (*node.Config, error) {
	config := node.Config{}

	config.ServerListPath = v.GetString(serverListKey)
	config.WalletsDir = v.GetString(walletsDirKey)

	if err := validatePath(config.ServerListPath); err != nil {
		return nil, fmt.Errorf("error opening %s, %v", config.ServerListPath, err)
	}

	if err := validatePath(config.WalletsDir); err != nil {
		return nil, fmt.Errorf("error opening %s, %v", config.WalletsDir, err)
	}

	port := v.GetString(httpPortKey)
	log.Error("Port number" + port)
	if port == "" {
		return nil, errors.New("HTTPPort not set in env")
	}

	// if validator wallet address is empty, error
	if v.GetString(validatorWalletAddressKey) == "" {
		return nil, errors.New("Validator wallet is not set in env config")
	}

	// Bootstrap default peer
	config.BootstrappingPeer = v.GetString(bootstrappingPeerKey)
	if config.BootstrappingPeer == "" {
		return nil, errors.New("Bootstrapping Peer is not set in env config")
	}
	config.Peers = append(config.Peers, &node.Peer{
		IP: config.BootstrappingPeer,
	})

	ip, err := utils.ExternalIP()
	if err != nil {
		return nil, err
	}

	config.IP = ip
	config.HTTPPort = port
	config.HTTPHost = v.GetString(httpHostKey)
	config.NodeType = v.GetString(nodeTypeKey)
	config.MaxBucketSize = v.GetInt(rateLimitBucketSizeKey)
	config.RateLimitCleanupInterval = v.GetInt(rateLimitCleanupIntervalKey)
	config.ValidatorWalletAddress = v.GetString(validatorWalletAddressKey)
	config.LIDBankWalletAddress = v.GetString(lidBankWalletAddressKey)
	config.LIDRewardSender = v.GetString(lidRewardSenderKey)
	config.BuildExecutable = v.GetString(lidExecutableKey)
	config.PriceDB = v.GetString(priceDBKey)

	return &config, nil
}

func lidNodeFlagSet() *flag.FlagSet {
	fs := flag.NewFlagSet("lid-network", flag.ContinueOnError)

	fs.String(httpHostKey, "localhost", "Specifies the network host of this node")
	fs.String(serverListKey, "server_list.json", "Specifies the server config file")
	fs.String(walletsDirKey, "data", "Directory with wallets data")
	fs.String(validatorWalletAddressKey, "", "The LID Bank wallet address")
	fs.String(lidBankWalletAddressKey, "lid1network", "The LID Bank wallet address")
	fs.String(lidRewardSenderKey, "7777", "The default sender address for network rewards")
	fs.String(lidExecutableKey, "LID-server", "The name of the build executable")
	fs.String(bootstrappingPeerKey, "", "The lid network peer to boostrap this node with")
	fs.String(priceDBKey, "price.fdb", "The label of the datastore to store pricing information")

	return fs
}

func validatePath(path string) error {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return fmt.Errorf("path %s doesn't exist", path)
	}
	return nil
}

func initJanitor() {
	// TODO: load config values from env
	jn := janitor.New(&janitor.Config{
		DispatchInterval: 1 * time.Hour,
		DirPath:          "./data",
		MaxBlocksDel:     100,
	})
	go jn.Dispatch()
}
