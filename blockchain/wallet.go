package blockchain

import (
	"bytes"
	"crypto/sha256"
	"encoding/gob"
	"encoding/hex"
	"errors"
	"fmt"
	"os"
	"time"

	// "os"

	"dau/models"
	"dau/utils"
	"github.com/syndtr/goleveldb/leveldb/opt"
)

// walletData represents a wallet data stored along its chain
type walletData struct {
	Tag       string
	CreatedAt time.Time
}

var datapath = "./temp/"

func (h *Handler) saveWalletData(tag, address string) error {
	path := walletDataPath + address
	data := walletData{
		Tag:       tag,
		CreatedAt: time.Now(),
	}

	var buf bytes.Buffer
	err := gob.NewEncoder(&buf).Encode(data)
	if err != nil {
		return err
	}

	err = h.WriteFile(path+"/data.bin", buf.Bytes(), 0700)
	if err != nil {
		return err
	}

	return nil
}

// CreateWallet ...
func (h *Handler) CreateWallet(req models.CreateWallet) (string, error) {
	var err error

	walletAddress := req.WalletAddress
	if !req.IsBroadcasted {
		walletAddress = fmt.Sprintf("QC%s", utils.StringWithCharset(32))
	}
	if h.WalletExists(walletAddress) {
		return walletAddress, err
	}

	// initail block
	var dataBuffer bytes.Buffer
	enc := gob.NewEncoder(&dataBuffer)
	block := Block{
		Amount:    0,
		Sender:    "00000000000",
		Reciever:  walletAddress,
		PrevHash:  "00000000000",
		CreatedAt: time.Now(),
	}

	// calculate hash
	stringForHash := block.PrevHash + fmt.Sprintf("%f", block.Amount) + walletAddress
	shaEngine := sha256.New()
	shaEngine.Write([]byte(stringForHash))

	block.Hash = hex.EncodeToString(shaEngine.Sum(nil))

	// hash supplied private key
	stringForHashRec := req.PrivateKey
	shaEngine2 := sha256.New()
	shaEngine2.Write([]byte(stringForHashRec))

	block.PrivateKeyHash = hex.EncodeToString(shaEngine2.Sum(nil))

	err = enc.Encode(block)
	if err != nil {
		return walletAddress, err
	}

	err = h.saveBlock(walletAddress, block)
	if err != nil {
		return walletAddress, err
	}

	err = h.saveWalletData(req.WalletName, walletAddress)

	return walletAddress, err
}

// CreateICOWallet ...
func (h *Handler) CreateICOWallet(privateKey string) error {
	walletAddress := "QCreserve"
	if h.WalletExists(walletAddress) {
		return nil
	}

	// initail block
	var dataBuffer bytes.Buffer
	enc := gob.NewEncoder(&dataBuffer)
	block1 := Block{
		Amount:    10000000000000,
		Sender:    "00000000000",
		Reciever:  walletAddress,
		PrevHash:  "00000000000",
		CreatedAt: time.Now(),
	}

	block2 := Block{
		Amount:    10000000000000,
		Sender:    "00000000000",
		Reciever:  walletAddress,
		PrevHash:  "00000000000",
		CreatedAt: time.Now(),
	}

	// calculate hash
	block1.Hash = genBlockHash(block1, walletAddress)

	// hash supplied private key
	stringForHashRec := privateKey
	shaEngine2 := sha256.New()
	shaEngine2.Write([]byte(stringForHashRec))

	block1.PrivateKeyHash = hex.EncodeToString(shaEngine2.Sum(nil))

	block2.PrevHash = block1.Hash
	block2.Hash = genBlockHash(block2, walletAddress)
	block2.PrivateKeyHash = hex.EncodeToString(shaEngine2.Sum(nil))

	for _, block := range []Block{block1, block2} {
		if err := enc.Encode(block); err != nil {
			return err
		}

		err := h.saveBlock(walletAddress, block)
		if err != nil {
			return err
		}
	}

	err := h.saveWalletData("qc_ico", walletAddress)
	if err != nil {
		return err
	}

	return nil
}

func genBlockHash(block Block, address string) string {
	stringForHash := block.PrevHash + fmt.Sprintf("%f", block.Amount) + address
	shaEngine := sha256.New()
	shaEngine.Write([]byte(stringForHash))

	return hex.EncodeToString(shaEngine.Sum(nil))
}

// Transfer ...
func (h *Handler) Transfer(data models.TrxData) (models.TrxData, error) {
	var tx models.TrxData
	// get senders previous block
	prevBlock, err := h.getLastBlock(data.Sender)
	if err != nil {
		return tx, err
	}

	prevBlockReciever, err := h.getLastBlock(data.Reciever)
	if err != nil {
		return tx, err
	}

	// Perform verifications for chains and private keys to confirm identity

	// validate sender chain
	vSenChain, err := h.VerifyChain(data.Sender)
	if err != nil {
		return tx, err
	}

	if !vSenChain {
		return tx, errors.New("Sender chain info not found")
	}

	//verify reciever chain()
	vRecChain, err := h.VerifyChain(data.Reciever)
	if err != nil {
		return tx, err
	}

	if !vRecChain {
		return tx, errors.New("Reciever chain info not found")
	}

	// verify senders identity with supplied private key
	vPrivateKey, err := h.VerifyPrivateKey(data.Sender, data.SPrivateKey)
	if err != nil {
		return tx, err
	}

	if !vPrivateKey {
		return tx, errors.New("Error verifying sender private key")
	}

	// open wallets

	ops := new(opt.Options)
	ops.NoSync = false

	// take money from the sender

	// check if there is enough money
	if !data.IsBroadcasted {
		if prevBlock.Amount < data.Amount {
			return tx, errors.New("Not enought funds for transfer")
		}
	}

	var dataBuffer bytes.Buffer
	enc := gob.NewEncoder(&dataBuffer)
	// var tempBlockHolder []Block
	var senderBlock Block
	if !data.IsBroadcasted {
		senderBlock.Amount = prevBlock.Amount - data.Amount
	}

	if data.IsBroadcasted {
		senderBlock.Amount = float64(data.SenderLastBlockAmount)
	}

	senderBlock.PrevHash = prevBlock.Hash
	senderBlock.Sender = data.Sender
	senderBlock.Reciever = data.Reciever
	senderBlock.PrivateKeyHash = prevBlock.PrivateKeyHash
	if !data.IsBroadcasted {
		senderBlock.ID = utils.StringWithCharset(12)
	}

	if data.IsBroadcasted {
		senderBlock.ID = data.SenderBlockID
	}

	// calculate hash
	stringForHash := senderBlock.PrevHash + fmt.Sprintf("%f", senderBlock.Amount) + data.Sender
	shaEngine := sha256.New()
	shaEngine.Write([]byte(stringForHash))
	senderBlock.Hash = hex.EncodeToString(shaEngine.Sum(nil))
	senderBlock.CreatedAt = time.Now()

	// add block to temp block holder set
	err = enc.Encode(senderBlock)
	if err != nil {
		return data, err
	}

	err = h.saveBlock(data.Sender, senderBlock)
	if err != nil {
		return data, err
	}

	// credit reciever
	var recieverBlock Block
	if !data.IsBroadcasted {
		recieverBlock.Amount = prevBlockReciever.Amount + data.Amount
	}
	if data.IsBroadcasted {
		recieverBlock.Amount = float64(data.RecieverLastBlockAmount)
	}
	recieverBlock.PrevHash = prevBlockReciever.Hash
	recieverBlock.Sender = data.Sender
	recieverBlock.Reciever = data.Reciever
	recieverBlock.PrivateKeyHash = prevBlockReciever.PrivateKeyHash

	if !data.IsBroadcasted {
		recieverBlock.ID = utils.StringWithCharset(12)
	}

	if data.IsBroadcasted {
		recieverBlock.ID = data.RecieverBlockID
	}

	// calculate hash
	stringForHashRec := recieverBlock.PrevHash + fmt.Sprintf("%f", recieverBlock.Amount) + data.Reciever
	shaEngineRe := sha256.New()
	shaEngineRe.Write([]byte(stringForHashRec))

	recieverBlock.Hash = hex.EncodeToString(shaEngineRe.Sum(nil))
	recieverBlock.CreatedAt = time.Now()

	var recBlockDataBuffer bytes.Buffer
	encDataRec := gob.NewEncoder(&recBlockDataBuffer)
	err = encDataRec.Encode(recieverBlock)
	if err != nil {
		return data, err
	}

	// save on disk
	err = h.saveBlock(data.Reciever, recieverBlock)
	if err != nil {
		return data, err
	}

	data.RecieverBlockID = recieverBlock.ID
	data.SenderBlockID = senderBlock.ID
	data.RecieverLastBlockAmount = recieverBlock.Amount
	data.SenderLastBlockAmount = senderBlock.Amount
	return data, err
}

// WalletInfo retrieves a summary for a wallet
func (h *Handler) WalletInfo(address string) (models.WalletInfo, error) {
	var info models.WalletInfo
	lastBlock, err := h.LastBlock(address)
	if err != nil {
		return info, err
	}

	info.Balance = fmt.Sprintf("%f", lastBlock.Amount)
	tag, err := h.getWalletMeta(address)
	if err != nil {
		return info, err
	}

	info.WalletName = tag.Tag
	info.WalletAddress = address

	return info, nil
}

// LastBlock retrieves the last block from a chain belonging to an address
func (h *Handler) LastBlock(address string) (Block, error) {
	return h.getLastBlock(address)
}

// WalletExists ... Check if a wallet exists on a single server
func (h *Handler) WalletExists(address string) bool {
	walletPath := walletDataPath + address + "/"
	_, errstat := os.Stat(walletPath)
	if os.IsNotExist(errstat) {
		return false
	}
	return true
}
