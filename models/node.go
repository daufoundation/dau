package models

import "time"

// BootstrapReq represents a Lid Node bootstrapping request
type BootstrapReq struct {
	IP     string `json:"ip"`
	PeerID string `json:"peer_id"`
}

// SyncRequest represents a node sync request
type SyncRequest struct {
	// Origin Node where sync data is dispatched to
	Origin string `json:"origin"`

	// Nodes that have synced with the Origin
	AcceptedNodes []string `json:"accepted_nodes"`
}

// SyncPayload represents the sync payload sent to nodes
type SyncPayload struct {
	// Blocks in this instance represents the wallet pairs:
	//				Wallet pair
	//				/				\
	//			walletAddress  walletTag
	Blocks []BlockData
}

// BlockData ...
type BlockData struct {
	WalletAddress  string    `json:"wallet_address"`
	PrivateKeyHash string    `json:"private_key_hash"`
	Hash           string    `json:"hash"`
	WalletTag      WalletTag `json:"wallet_tag"`
}

// WalletTag ...
type WalletTag struct {
	Tag       string    `json:"tag"`
	CreatedAt time.Time `json:"created_at"`
}

// PriceInfo represents the price information configured on a Node
type PriceInfo struct {
	CurrentPrice float64 `json:"current_price"`
	Volume24H    float64 `json:"volume_24h"`
}

// PriceSeriesData represents the price series data format
type PriceSeriesData struct {
	Data        [][]interface{}
	PriceChange float64
}
