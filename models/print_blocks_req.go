package models

// PrintBlockReq ...
type PrintBlockReq struct {
	PrivateKey string `json:"private_key"`
	Address    string `json:"address"`
}

// WalletInfoReq ...
type WalletInfoReq struct {
	Address string `json:"address"`
}

// WalletInfo ...
type WalletInfo struct {
	WalletName    string `json:"wallet_name"`
	WalletAddress string `json:"wallet_address"`
	Balance       string `json:"balance"`
}
