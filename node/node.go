package node

import (
	"bytes"
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"net/http"
	"os"
	"time"

	"dau/database"
	"dau/models"
	safebytes "dau/utils/bytes"
	"dau/utils/logging"
)

// Node is an instance of a Lid Node
type Node struct {
	Log        logging.Logger
	LogFactory logging.Factory

	Config *Config

	// Node's unique ID used in communicating with other nodes on the network
	// TODO: switch to ID dataset
	ID string

	closer chan<- os.Signal
}

// Peer represents a Lid Node connected on the network
type Peer struct {
	IP string
	ID string
}

var (
	appName = "lid-network"
	// DefaultDir home dir
	DefaultDir = fmt.Sprintf("~/.%s/", appName)
)

// price updater job interval.
// Set price updater interval in HH:MM:SS format
const (
	priceUpdaterPeriod time.Duration = 24 * time.Hour
	priceUpdateH       int           = 00
	priceUpdateM       int           = 00
	priceUpateS        int           = 00
)

type priceUpdaterTicker struct {
	timer *time.Timer
}

// Initialize the Lid node
func (n *Node) Initialize(config *Config, log logging.Logger, logFactory logging.Factory) error {
	n.Config = config
	n.Log = log
	n.LogFactory = logFactory

	// initialize node dependencies
	if err := n.initBuildID(); err != nil {
		return fmt.Errorf("failed to initialize build hash: %v", err)
	}

	// add node ip to peer list (server_list.json)
	if err := n.addNodeToPeers(); err != nil {
		return fmt.Errorf("failed to add node-id to peerlist: %v", err)
	}

	return nil
}

// Dispatch runs the node's required services
func (n *Node) Dispatch() error {
	// Track bootstrap nodes
	n.Log.Info("sending bootstrapping information to bootstrap node(s)")
	// TODO: update when ip implementation is up
	addr := n.connAddr()
	n.Log.Info("this node's IP is set to: %s", addr)
	req := models.BootstrapReq{
		IP: addr,
	}
	for _, peer := range n.Config.Peers {
		n.track(peer.IP, req)
	}

	// start node sync
	if err := n.syncBlocks(addr, n.Config.BootstrappingPeer); err != nil {
		return err
	}

	return nil
}

// initializeBuildID for this node
func (n *Node) initBuildID() error {
	executable, _ := ioutil.ReadFile(n.Config.BuildExecutable)
	// if err != nil {
	// 	return err
	// }

	str := string(executable)
	sha := sha256.New()
	sha.Write([]byte(str))
	hash := hex.EncodeToString(sha.Sum(nil))

	n.Config.BuildHash = hash

	return nil
}

// PriceUpdater runs a scheduled job to update the current price configured on
// a Node.
func (n *Node) PriceUpdater(priceDB, transactionsDB database.Database) {
	ticker := &priceUpdaterTicker{}
	ticker.updateTimer()
	for {
		<-ticker.timer.C
		err := n.updateTokenPrice(priceDB, transactionsDB)
		if err != nil {
			n.Log.Error("failed to update token price: %v", err)
			return
		}
		ticker.updateTimer()
	}
}

func (n *Node) updateTokenPrice(priceDB, transactionsDB database.Database) error {
	txData, err := transactionsDB.Get([]byte("transactions_count"))
	if err != nil {
		return err
	}

	priceData, err := priceDB.Get([]byte("price"))
	if err != nil {
		return err
	}

	currentPrice := float64frombytes(priceData)
	txCount := float64frombytes(txData)

	// derive percentage
	// formula:
	// % = txCount * 1.7 * 10^-8
	updatePerc := (txCount * float64(1.7)) * math.Pow10(-8)
	percDerivedPrice := updatePerc * currentPrice
	newPrice := currentPrice + percDerivedPrice
	newPriceBytes := float64bytes(newPrice)

	// persit update
	err = priceDB.Put([]byte("price"), newPriceBytes)
	if err != nil {
		return err
	}

	series, err := priceDB.Get([]byte("price_series"))
	if err != nil {
		return err
	}
	var seriesData models.PriceSeriesData
	if err := safebytes.UnmarshalBytes(series, &seriesData); err != nil {
		return err
	}

	// add new price series data
	seriesData.PriceChange = updatePerc
	seriesData.Data = append(seriesData.Data, []interface{}{
		int64(time.Now().Unix()),
		newPrice,
	})

	newSeriesB, err := safebytes.MarshalBytes(seriesData)
	if err != nil {
		return err
	}

	if err := priceDB.Put([]byte("price_series"), newSeriesB); err != nil {
		return fmt.Errorf("failed to update price_series: %v", err)
	}

	n.Log.Info("Token price has been updated. new_price=%v old_price=%v", newPrice, currentPrice)

	return nil
}

// updateTimer sets the timer to tick on next interval
func (t *priceUpdaterTicker) updateTimer() {
	nextTick := time.Date(time.Now().Year(), time.Now().Month(),
		time.Now().Day(), priceUpdateH, priceUpdateM, priceUpateS, 0, time.Local)
	if !nextTick.After(time.Now()) {
		nextTick = nextTick.Add(priceUpdaterPeriod)
	}

	diff := nextTick.Sub(time.Now())
	if t.timer == nil {
		t.timer = time.NewTimer(diff)
	} else {
		t.timer.Reset(diff)
	}
}

func (n *Node) addNodeToPeers() error {
	var peers []string

	if n.Config.NodeType == "local" {
		return nil
	}

	f, err := os.Open("server_list.json")
	if err != nil {
		return err
	}
	defer f.Close()

	err = json.NewDecoder(f).Decode(&peers)
	if err != nil {
		return err
	}

	if contains(peers, n.Config.IP) {
		return nil
	}

	peers = append(peers, n.Config.IP)
	// update list
	f2, err := os.OpenFile("server_list.json", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		return err
	}
	defer f2.Close()

	if err := json.NewEncoder(f2).Encode(peers); err != nil {
		return err
	}

	return nil
}

// syncBlocks starts a wallet blocks sync chain from the connected bootstrap
// peer to its network of peers
//
// Initial sync payload is sent to bootstrapping peer, and the blocks are
// fowarded to the requesting node, this processes is repeated when other peers
// receive the payload
func (n *Node) syncBlocks(node, bootstrapPeer string) error {
	n.Log.Info("Bootstrapping started syncing blocks")
	// prepare payload
	payload := models.SyncRequest{
		Origin: node,
	}

	if node == bootstrapPeer {
		return nil
	}

	go func(peer string, payload models.SyncRequest) {
		data, _ := json.Marshal(payload)
		b := bytes.NewBuffer(data)
		endpoint := fmt.Sprintf("http://%s/node/sync", peer)

		r, err := makeRequest(n.Config.BuildHash, "POST", endpoint, b)
		if err != nil {
			n.Log.Error("node sync request failed: %v", err)
		}
		if r != nil {
			r.Body.Close()
		}
	}(bootstrapPeer, payload)

	return nil
}

// track sends a bootstrap request to peer
// registers node as part of receieving node's peers
// TODO: move method to network util?
func (n *Node) track(peer string, req models.BootstrapReq) {
	if peer == req.IP {
		return
	}

	go func(peer string) {
		data, _ := json.Marshal(req)
		e := fmt.Sprintf("http://%s/node/gossip-peer", peer)
		resp, err := makeRequest(n.Config.BuildHash, "POST", e, bytes.NewBuffer(data))
		if err != nil {
			n.Log.Error("failed to track bootstrap peer, err: %v", err)

		}

		if resp == nil {
			return
		}

		if resp.StatusCode >= 300 {
			n.Log.Debug("failed to track bootstrap peer, status=%s", resp.Status)
			return
		}

		if resp != nil && resp.Body != nil {
			resp.Body.Close()
		}

		// retrieve and save peer list from bootstrapping peer
		var respData struct {
			Status string   `json:"status"`
			Peers  []string `json:"peers"`
		}
		_ = json.NewDecoder(resp.Body).Decode(&respData)
		err = n.persistPeerList(respData.Peers)
		if err != nil {
			n.Log.Error("failed to persist peerlist from bootstrap: %v", err)
			return
		}

		n.Log.Info("tracked broadacasted peer info from %s", peer)
		return
	}(peer)
}

func (n *Node) persistPeerList(list []string) error {
	f, err := os.OpenFile("server_list.json", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		return err
	}

	defer f.Close()

	if err := json.NewEncoder(f).Encode(list); err != nil {
		return err
	}

	return nil
}

func makeRequest(buildID string, method, endpoint string, body io.Reader) (*http.Response, error) {
	client := &http.Client{Timeout: time.Second * 10}
	req, err := http.NewRequest(method, endpoint, body)
	if err != nil {
		return nil, err
	}
	req.Close = true
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("X-LID-BUILD", buildID)
	req.Header.Add("X-LID-Outbound", "broadcast")
	return client.Do(req)
}

func (n *Node) connAddr() string {
	var addr string
	addr = fmt.Sprintf("%s:%s", n.Config.HTTPHost, n.Config.HTTPPort)

	if n.Config.NodeType != "local" && n.Config.IP != "" {
		addr = n.Config.IP
	}

	return addr
}

func contains(s []string, i string) bool {
	for _, a := range s {
		if a == i {
			return true
		}
	}
	return false
}

func float64bytes(f float64) []byte {
	bits := math.Float64bits(f)
	bytes := make([]byte, 8)
	binary.LittleEndian.PutUint64(bytes, bits)
	return bytes
}

func float64frombytes(bytes []byte) float64 {
	bits := binary.LittleEndian.Uint64(bytes)
	float := math.Float64frombits(bits)
	return float
}
