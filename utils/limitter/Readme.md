# Rate Limitter util
To load test API handlers, a recommended tool to use is the Apache Benchmark
(ab) tool. Download here https://httpd.apache.org/docs/2.4/programs/ab.html

## Sample Load Test
```
$ ab -n 100 -c 10 http://localhost:300/api/v1/
```
