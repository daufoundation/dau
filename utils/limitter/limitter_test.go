package limitter

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func BenchmarkLimitMiddleware(b *testing.B) {
	rlimitter := New(1, 1)

	testHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		io.WriteString(w, `{"alive": true}`)
	})

	for i := 0; i < b.N; i++ {
		middlewareHandler := rlimitter.Limit(testHandler)
		req := httptest.NewRequest("GET", "http:localhost/api/v1", nil)
		middlewareHandler.ServeHTTP(httptest.NewRecorder(), req)
	}
}

func BenchmarkSessionRetrieval(b *testing.B) {
	rlimitter := New(1, 1)

	for i := 0; i < b.N; i++ {
		rlimitter.getSession(string(i))
	}
}
