package logging

import (
	"fmt"
	"strings"
)

// Level represents a Log Level
type Level int

const (
	Off Level = iota
	Fatal
	Error
	Warn
	Verbose
	Info
	Debug
)

func (l Level) String() string {
	switch l {
	case Fatal:
		return "FATAL"
	case Error:
		return "ERROR"
	case Warn:
		return "WARN "
	case Info:
		return "INFO "
	case Debug:
		return "DEBUG"
	case Verbose:
		return "VERBOSE"
	default:
		return "?????"
	}
}

// ToLevel returns the level representation
func ToLevel(l string) (Level, error) {
	switch strings.ToUpper(l) {
	case "OFF":
		return Off, nil
	case "FATAL":
		return Fatal, nil
	case "ERROR":
		return Error, nil
	case "WARN":
		return Warn, nil
	case "INFO":
		return Info, nil
	case "DEBUG":
		return Debug, nil
	case "VERBOSE":
		return Verbose, nil
	default:
		return Info, fmt.Errorf("unknown log level: %s", l)
	}
}
