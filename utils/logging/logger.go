package logging

// Logger defines the interface that is used to keep a log of all events that
// happen
type Logger interface {
	Fatal(format string, args ...interface{})
	Error(format string, args ...interface{})
	Warn(format string, args ...interface{})
	Info(format string, args ...interface{})
	Debug(format string, args ...interface{})
	Verbo(format string, args ...interface{})
}
