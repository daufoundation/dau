package utils

import "net"

// ExternalIP returns the external IP for this node
func ExternalIP() (string, error) {
	var ip string
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return ip, err
	}

	for _, address := range addrs {
		// check the address type and if it is not a loopback the display it
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String(), nil
			}
		}
	}

	return ip, nil
}
