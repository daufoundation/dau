package snag

const (
	// OpNoSender occurs when a transfer operation contains no sender or sender is
	// not found
	OpNoSender = "op_no_sender"
	// OpNoRecipient occurs when a transfer operation contains no recipient or
	// recipient is not found
	OpNoRecipient = "op_no_recipient"
	// OpBlockNotExist occurs when wallet block does not exist
	OpBlockNotExist = "op_block_not_exist"

	// TxFailed occurs when a chain transfers is not successful
	TxFailed = "tx_failed"
)
